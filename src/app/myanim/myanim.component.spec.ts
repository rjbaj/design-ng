import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyanimComponent } from './myanim.component';

describe('MyanimComponent', () => {
  let component: MyanimComponent;
  let fixture: ComponentFixture<MyanimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyanimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyanimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
