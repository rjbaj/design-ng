import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {

  @Input()
  following: string;
  @Input()
  followers: string;
  @Input()
  points: string;

  constructor() {
    this.following = '0';
    this.followers = '0';
    this.points = '0';
  }

  /**
   * 100,000 becomes 100K 100,000,000 becomes 100M
   * @param val
   */
 private reshapeValue(val: string): string { // TODO use as pipe
   console.log(val)
   if (val.length > 3 && val.length <= 6) {
     val = val.slice(0, 2) + 'K'
   } else if (val.length > 6 && val.length <= 9) {
     val = val.slice(0, 2) + 'M'
   } else {
     val = '?reshapevalue'
   }
   return val
 }


}
