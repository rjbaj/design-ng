import { Component, OnInit } from '@angular/core';
import { group, trigger, state, style, animate, transition } from '@angular/animations';
import { animateFactory } from 'ng2-animate';

@Component({
  selector: 'app-myanim',
  templateUrl: './myanim.component.html',
  styleUrls: ['./myanim.component.scss'],
  animations: [animateFactory(300, 0, 'ease-in') ]
})
export class MyanimComponent implements OnInit {

  state = 'pulse'
  img = 'assets/img/0.jpg'
  img2 = 'assets/img/1.jpg'

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.state = this.state === 'pulse' ? 'flash' : 'pulse';
  }

  animStart() {
    console.log('animstart')
  }

  animEnd() {
    console.log('animEnd')
  }

}
