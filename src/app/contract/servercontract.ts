import { RequestOptions, RequestMethod, Headers } from '@angular/http'

export const env = 'http://mikhaa.com/api/'
const basicHeader = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Access-Control-Allow-Origin': 'http://mikhaa.com'
}


export const api = {
  'refresh': {
    url: env + 'refresh',
    method: RequestMethod.Post,
    auth: false,
    headers: basicHeader
  },
  'login': {
    url: env + 'user/login',
    method: RequestMethod.Post,
    auth: false,
    headers: basicHeader
  },
  'userinfo': {
    url: env + 'user/info',
    method: RequestMethod.Get,
    auth: true,
    headers: basicHeader
  }
}
