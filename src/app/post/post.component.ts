import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  post: Post;
  comments: Comment[]

  constructor() {
    this.post = {
      avatar: 'assets/image.jpg',
      address: 'Sydney, NSW',
      creator: 'XploreSydney',
      title: 'Title of post',
      exerpt: 'One of my favourite places to fish. Reasons being: it is a comfortable place to fish with seats and a long jetty and ...',
      hostimg: 'assets/img/0.jpg',
      likesCount: '5k',
      commentsCount: '891'
    }
    this.comments = [
      { user: 'superM0nkey', comment: 'Great write up!' },
      { user: 'Denise', comment: 'Appreciate the details! will definitely use this as a guide! xx' },
      { user: 'Ash_ketchup', comment: 'Imma catch dem all.' }
    ];
  }

  ngOnInit() {
  }

  getTime(): string {
    return '23 HOURS AGO';
  }

}

interface Comment {
  user: string;
  comment: string;
}

interface Post {
  avatar: string;
  creator: string;
  address: string;
  hostimg: string;
  title: string;
  exerpt: string;
  likesCount: string;
  commentsCount: string;
}
