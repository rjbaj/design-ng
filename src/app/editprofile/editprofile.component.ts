import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { InterceptedHttp, InterceptArgs } from 'app/providers/intercepted-http.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss'],
  providers: [InterceptedHttp]
})
export class EditprofileComponent implements OnInit {

  private username= 'rajib1'
  private password= 'rajib1'

  constructor(private http: InterceptedHttp) {
  }

  ngOnInit() {
  }

  login() {
    const args: InterceptArgs = {
      urlname: 'login',
      body: { 'username': this.username, 'password': this.password }
    }
    this.http.req(args)
      .then(result => {
        console.log('success')
        console.dir(result)
      }).catch(err => {
        console.log('err')
        console.dir(err)
      })
  }

}
