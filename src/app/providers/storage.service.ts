import { Injectable } from '@angular/core';

@Injectable()
export class PromiseStorage {

  // tslint:disable-next-line:max-line-length
  readonly access = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQyNGE0Njc5ZGFiMzA4MDQwOTRiYTkxMTdkY2FiOWFmYjdmYzdlM2U3M2ZiOWQxNTk5MGZiMWMwYWIzZjZjNTRkYzU1YTQ5ZDRhMGU3ZWNiIn0.eyJhdWQiOiIyIiwianRpIjoiZDI0YTQ2NzlkYWIzMDgwNDA5NGJhOTExN2RjYWI5YWZiN2ZjN2UzZTczZmI5ZDE1OTkwZmIxYzBhYjNmNmM1NGRjNTVhNDlkNGEwZTdlY2IiLCJpYXQiOjE1MDM1NjI1NjgsIm5iZiI6MTUwMzU2MjU2OCwiZXhwIjoxNTM1MDk4NTY4LCJzdWIiOiIxOSIsInNjb3BlcyI6W119.ChaBJF2YSTEBQ-0X6vi2aZplzAC5OcJhL4b1iOa9cElkm9_ZESnlNh2A6qdbll5g5nzzuR6oZMRSCpg4-uEFHCqxjyAr_BpetCfWz57VxlynlUOVmdX2Zo9x-ssPowlbYEI3As0n-BudvU9MzzGq5mJkjGjP7XL_f0jLDHBqC4zAkyXs_SQCTKXey5MfA2nebrZ9n8qba8HqEbzttUv_aBRRa5czCwUOi7I7gksyLU-m6CE2LNMdztnpCOcKw77Q6-iZjfIKLPp2hL_orsJUZ7e2wpQdcPVeuSMLwkuQWiQpXG9kZ77jCGqQIyicGmKLGbd5MLjnV96ETD05iSVYQqgPUpqiHgxZVx-onRFKWUkQ-uaW1QGTC85MD9G5Por3Wj7Vi3U-C-vdpqyZEAxz6_o_fq65wHwd7tS1e3jdulZ6Hm6Azsr1CxN0T1TtaxFTNnpcyMP0btsr6fC2sbcFWNpI7hUS69jBLcPPP7zBbcV10rxYmjmOg2Yx6YaSxJ2F1uYXG7bkkUhsNALVhaNbJ8myQLtrYrsvV3zqbYgOCNxQ7faTJZkx7RjFYj7OL4BfD6YboO2o4A5v7VQHIWUcynCVZ-8trvaFr691F_8jQ2lsmaUbWJjPtIGtkpUCEypxDuWEM3cwnvRMrG8CVi25qNlvUxadvHMhPJWvZmXU0OI","refresh_token":"h0UO2WfbfxN0FK8nIKmoMDS9qD2i4uZxSz4NfkUL6emnG0bVuxsSLrehnAEFaRmKk6DYSoROSiMFGFHGAGzLYEmjE\/R4aDufBSdpTLDI1UTk+S0brGS8aVhIS2Sg\/zozkc5FdlVaX5nbxa54rzbF7CPjC2AE6SWIIWMNPuLiGom3A6sWVAb4PaJiccuZAqyev28SCJdrCn2Rp5BXQSbRDN3x\/6TFjOYGVaTtq8zKK+IbWsnMXCfq8bw\/yKXwXoa3sRWgUAYZJkmiGLCNOYuhzAoaTUYJ6rXLeb35ROMxBcyYkC8tzt2mH9xUGFkQnNUZWh0jVwS8jqU9bzmncLwxxdiq1fzyqBZK\/swtHZIsEHGL8bvw3wMm8ctNOGC2qqzCM+GOLEAfk0K0E0KDrSoTvEj4Ydq0p9Xc6eYbHhVqWR10T3qsXzU\/qoPBacNvWi1C+6X7IC3i0NjbIyoFAtYojrsBe4zz6BTHyg01APssLbcsp24anEVTXzBLpajTefPOJOQ\/1fOy93has08eK8jgrmHFS1dm7dc1ArBDtW\/LvUruZmL8kSAOogC+rNIhx2CxGNcNQtpyE\/av+HHoIFbq3a+S44LBSJlJKs53Ljz0M0M1QsttO7gn2Bty9TrrkLBQWEEEDlH7u\/52jl2lmOFPunQhVdEoac\/FYs\/mA1yFKqc='
  readonly refresh = ''

  public getAuth(): Promise<any> {
    return Promise.all([
      Promise.resolve('Bearer'),
      Promise.resolve(this.access),
    ]).then(values => {
      if (!values[0] || !values[1]) {
        return Promise.reject('token_type or acces_token are not valid, ' + values)
      }
      return Promise.resolve(values[0] + ' ' + values[1])
    })
  }

  public getRefresh(): Promise<any> {
    return Promise.all([
      Promise.resolve('Bearer'),
      Promise.resolve(this.refresh),
    ]).then(values => {
      if (!values[0] || !values[1]) {
        return Promise.reject('token_type or acces_token are not valid, ' + values)
      }
      return Promise.resolve(values[0] + ' ' + values[1])
    })
  }
}
