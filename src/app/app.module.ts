import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component'
import { StatisticsComponent } from './statistics/statistics.component'
import { ProfileComponent } from './profile/profile.component'
import { PostComponent } from './post/post.component'
import { MyanimComponent } from './myanim/myanim.component'
import { EditprofileComponent } from './editprofile/editprofile.component'
import { UserserviceComponent } from './userservice/userservice.component'
import { RequestOptions, Http, XHRBackend, HttpModule } from '@angular/http'
import { InterceptedHttp } from 'app/providers/intercepted-http.service'
import { PromiseStorage } from 'app/providers/storage.service'

export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, storage: PromiseStorage): InterceptedHttp {
  return new InterceptedHttp(xhrBackend, requestOptions, storage)
}

@NgModule({
  declarations: [
    AppComponent,
    StatisticsComponent,
    PostComponent,
    ProfileComponent,
    MyanimComponent,
    EditprofileComponent,
    UserserviceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    PromiseStorage,
    {
      provide: Http,
      useFactory: interceptorFactory,
      deps: [XHRBackend, RequestOptions, PromiseStorage]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
