import { DesignPage } from './app.po';

describe('design App', () => {
  let page: DesignPage;

  beforeEach(() => {
    page = new DesignPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
