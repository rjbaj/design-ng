import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  image = 'assets/img/0.jpg'
  avatar = 'assets/image.jpg'

  constructor() { }

  ngOnInit() {
  }

}
