import { Injectable } from '@angular/core'
import { Headers, Http, RequestOptions, RequestOptionsArgs, RequestMethod, Response, XHRBackend } from '@angular/http'
import 'rxjs/add/operator/toPromise'
import { Observable } from 'rxjs/Observable'

import { api } from 'app/contract/servercontract';
import { PromiseStorage } from 'app/providers/storage.service';


export interface InterceptArgs {
  urlname: string,
  params?: any,
  body?: any,
  headers?: any,
  errorhandle?: [{
    status: number,
    handle: (error: any) => void
  }]
}

export class HandledError {
  constructor(private status: number, private err: any){}
}

// make storage independent and memory dependent
@Injectable()
export class InterceptedHttp extends Http {

  constructor(_backend: XHRBackend, _defaultOptions: RequestOptions, private storage: PromiseStorage) {
    super(_backend, _defaultOptions)
  }

  async req(ropts: InterceptArgs): Promise<Response> {
    const a = api[ropts.urlname]
    if (!a) {
      throw new Error('wrongful request made')
    }
    const options: RequestOptionsArgs = {}
    options.method = a.method
    if (ropts.headers) {
      options.headers = Object.assign(a.headers, 

    }
    if (a.auth) {
      try {
        options.headers = Object.assign(options.headers, await this.storage.getAuth())
      } catch (err) {
        throw new Error(err)
      }
    }
    if (a.method === RequestMethod.Post) {
      options.body = ropts.body
      if (ropts.params) {
        options.params = ropts.params
      }
    } else if (a.method === RequestMethod.Get) {
      options.params = ropts.params
    }

    console.log('options: ')
    console.dir(options)
    return new Promise<Response>((resolve, reject) => {
      return super.request(a.url, options).subscribe(
        (value: Response) => {
          resolve(value)
        },
        (error: any) => {
          let handled = false
          if (ropts.errorhandle) {  // if custom err handlers are present then
            for (const eh of ropts.errorhandle) {
              if (error.status === eh.status) {
                eh.handle(error)
                handled = true
              }
            }
          }
          if (!handled) {
            reject(this.err(error, a))
          }
          reject(error)
        }
      )
    })
  }



  err(error: any, a: any): Promise<any> | HandledError {
    console.log('error: ' + error)
    if (error.status === 0) {   // no response
      console.log('status = 0')
      return new HandledError(error.status, error)
    } else if (error.status === 401) {
      console.log('status === 401')
      if (a.auth === true) {
        // make a refresh token and send another request to get it.
        this.storage.getRefresh()
          .then(value => {
            // re-request this request
            const ropts: InterceptArgs = {
              urlname: 'refresh' 
            } 
            this.req()
            return this.req(a)
          }).catch(err => {
            // critical
            return new HandledError(error.status, error)
          })
      }
    } else {
      console.dir(error)
    }
  }


}


